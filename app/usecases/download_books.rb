=begin

=end

require_relative('../models/book.rb')
require_relative('../helpers/http_request_helper.rb')

include HttpRequestHelper

DB_ENV ||= 'development'
connection_details = YAML::load(File.open('config/database.yml'))
ActiveRecord::Base.establish_connection(connection_details[DB_ENV])

@response = HttpRequestHelper::do_http_get("https://api.nytimes.com/svc/books/v3/lists/2018-01-07/hardcover-fiction.json?api-key=kbGfqehGvpKoNXI2BETek2JmsRDQvh5f")

for item in @response.results.books do


  puts "..fetch book records from server"
  puts item.author + "\n"

  Book.create(title:item.title,
              author:item.author,
              position:item.rank,
              weeks:item.weeks_on_list)


end