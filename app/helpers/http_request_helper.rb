=begin

=end


require 'httparty'
# OpenStruct is not included by default so you have to add it.
require 'ostruct'

module HttpRequestHelper

  def do_http_get(_url)

    puts "..making http request"

    _response = HTTParty.get(_url,
                             headers: {'Accept' => 'application/json'})


    json_object = JSON.parse(_response.body, object_class: OpenStruct)


  end

end
