# README

The goal for this sample app was to simply fulfil the instructions as described below:

Using the New York Times books API (https://developer.nytimes.com/books_api.json), please retrieve the hardcover fiction bestseller list that was published on January 7, 2018, save the data in some way, and then display the top 10 on a page (book title, author, position, and weeks on the list).

The following should therefore be noted for the review.

1. The data is downloaded from the server via a direct call to the books api and the records inserted into a postgresql database. The code for this task was ran from command line and can be found in app/usecases/download_books.rb
2. The records stored in the books table is then retrieved using rails/activerecord and displayed as requested. The code resides in the books_controller.rb and index.html.erb
3. The code generally is not production ready as was adviced.